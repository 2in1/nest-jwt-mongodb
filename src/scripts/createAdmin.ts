import { connect, disconnect } from '../services/mongo.service';
import { UserModel } from '../models/user.model';
import { UserRole } from '../constants/user';
import { hash } from '../services/crypto.service';

(async function createAdmin(): Promise<void> {
    try {
        connect();

        const data = {
            email: 'admin1@gmail.com',
            password: await hash('Admin@123'),
            name: 'Admin',
            role: UserRole.ADMIN,
        };
        const user = await UserModel.create(data);
        console.log('INFO | Create admin DONE !', user.email);
        disconnect();
    } catch (err) {
        console.error(err);
        disconnect();
    }
})();
