import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { IUserDocument } from '../user/user.interface';
import { makeJWT } from 'src/services/jwt.service';
import { verify } from 'src/services/crypto.service';
import { MODEL } from 'src/constants/database';
import { config } from 'src/constants/config';
import { IToken } from './auth.interface';
import { ForgotPasswordDto } from './dto/auth.dto';
import { RedisService } from 'src/core/redis/redis.service';

@Injectable()
export class AuthService {
    constructor(
        @Inject(MODEL.USER) private readonly userModel: Model<IUserDocument>,
        private readonly redis: RedisService,
    ) {}

    async login(user: IUserDocument): Promise<IToken> {
        const token = makeJWT({ sub: user.id }, { expiresIn: config.JWT_EXPIRED });
        await this.redis.set(user.id, token);
        return { token };
    }

    async validatePassword(password: string, hashed: string): Promise<boolean> {
        const result = await verify(password, hashed);
        return result;
    }

    async forgotPassword(email: ForgotPasswordDto): Promise<any> {
        return email;
    }

    async logout(user: IUserDocument): Promise<number> {
        return await this.redis.del(user.id);
    }
}
