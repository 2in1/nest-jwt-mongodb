import { UserModule } from './../user/user.module';
import { AuthService } from './auth.service';
import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { DatabaseModule } from 'src/database/database.module';
// import { PassportModule } from '@nestjs/passport';
import { RedisModule } from 'src/core/redis/redis.module';
import { UserProviders } from 'src/models/user.model';

@Module({
    imports: [DatabaseModule, RedisModule, UserModule],
    controllers: [AuthController],
    providers: [AuthService, UserProviders],
    exports: [AuthService],
})
export class AuthModule {}
