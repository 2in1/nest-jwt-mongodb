import { UserService } from '../user/user.service';
import { Body, Controller, Post, UseGuards, Delete, Param } from '@nestjs/common';
import { LoginDto, ForgotPasswordDto, SetPasswordDto } from './dto/auth.dto';
import { CreateUserDto } from '../user/dto/user.dto';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from 'src/core/guards/jwt.guard';
import { successResponse, error400Response, IResponse } from 'src/services/response.service';
import { IUserDocument } from '../user/user.interface';
import { User } from '../../core/decorators/user.decorator';

@Controller()
export class AuthController {
    constructor(private readonly auth: AuthService, private readonly user: UserService) {}

    @Post('login')
    async index(@Body() data: LoginDto): Promise<IResponse> {
        const user = await this.user.findByEmail(data.email);
        if (!user) return error400Response('Email not register !');

        const verified = await this.auth.validatePassword(data.password, user.password);
        if (!verified) return error400Response('Email or password incorrect !');

        const response = await this.auth.login(user);
        return successResponse(response);
    }

    @Post('register')
    async register(@Body() data: CreateUserDto): Promise<IResponse> {
        const user = await this.user.findByEmail(data.email);
        if (user) return error400Response('Email has exist !');

        const response = await this.user.create(data);
        return successResponse(response);
    }

    @Post('forgot-password')
    async forgotPassword(@Body() data: ForgotPasswordDto): Promise<IResponse> {
        const user = await this.user.findByEmail(data.email);
        if (user) return error400Response('Email has exist !');

        const response = await this.auth.forgotPassword(data);
        return successResponse(response);
    }

    async verifyToken(@Param('token') token: string): Promise<IResponse> {
        console.log(token);
        return successResponse(token);
    }

    async setPassword(@Param('token') token: string, @Body() data: SetPasswordDto): Promise<IResponse> {
        console.log(token);
        console.log(data);
        return successResponse(data);
    }

    @Delete('logout')
    @UseGuards(JwtAuthGuard)
    async logout(@User() user: IUserDocument) {
        const result = await this.auth.logout(user);
        return successResponse(result === 1);
    }
}
