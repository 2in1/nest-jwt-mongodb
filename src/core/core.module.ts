import { Module } from '@nestjs/common';
import { APP_INTERCEPTOR, APP_FILTER, APP_GUARD } from '@nestjs/core';
import { MorganInterceptor } from 'nest-morgan';
import { HttpExceptionFilter } from './filters/exception.filter';
import { JwtStrategy } from './strategies/jwt.strategy';
import { DatabaseModule } from 'src/database/database.module';
import { RedisModule } from 'src/core/redis/redis.module';
import { UserProviders } from 'src/models/user.model';

// import { LoggingInterceptor } from './interceptors/logging.interceptor';
// import { TransformInterceptor } from './interceptors/transform.interceptor';

@Module({
    imports: [DatabaseModule, RedisModule],
    providers: [
        {
            provide: APP_INTERCEPTOR,
            useClass: MorganInterceptor('combined'),
        },
        {
            provide: APP_FILTER,
            useClass: HttpExceptionFilter,
        },
        {
            provide: APP_GUARD,
            useClass: JwtStrategy,
        },
        UserProviders,
        // { provide: APP_INTERCEPTOR, useClass: LoggingInterceptor },
        // { provide: APP_INTERCEPTOR, useClass: TransformInterceptor },
    ],
})
export class CoreModule {}
