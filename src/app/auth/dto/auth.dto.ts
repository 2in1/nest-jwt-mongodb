import { IsNotEmpty, IsString } from 'class-validator';

export class LoginDto {
    @IsNotEmpty()
    @IsString()
    readonly email: string;

    @IsNotEmpty()
    @IsString()
    readonly password: string;
}

export class ForgotPasswordDto {
    @IsNotEmpty()
    @IsString()
    readonly email: string;
}

export class SetPasswordDto {
    @IsNotEmpty()
    @IsString()
    readonly password: string;

    @IsNotEmpty()
    @IsString()
    readonly confirmPassword: string;
}
