export class Response {
    VALIDATION_ERROR = 'Validation Invalid';
    COMMON_ERROR = 'Error';
    UNAUTHORIZED_ERROR = 'Unauthorized';
    UNAUTHENTICATED_ERROR = 'Unauthenticated';
    BAD_REQUEST_ERROR = 'Bad Request';
    NOT_FOUND_ERROR = 'Not Found';
    INTERNAL_SERVER_ERROR = 'Internal Server Error';
    TOKEN_EXPIRED_ERROR = 'Token Expired';
    SUCCESSFUL = 'Success';
}
