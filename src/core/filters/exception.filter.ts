/* eslint-disable @typescript-eslint/no-unused-vars */
import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common';
import { Request, Response } from 'express';

type ExceptionType = {
    status: number;
    message: string;
    response: {
        error?: any;
        message?: string[] | string;
    };
};

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
    catch(exception: ExceptionType, host: ArgumentsHost): void {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const request = ctx.getRequest<Request>();
        const status = exception.status;
        const data = exception.response.error;

        response.status(status).json({
            statusCode: status,
            message: exception.response.message || exception.message,
            data,
        });
    }
}
