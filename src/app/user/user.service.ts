import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { Paging } from '../../common/types/paging';
import { hash } from 'src/services/crypto.service';
import { IUserDocument, TCreateUser, TUpdateUser } from './user.interface';
import { MODEL } from 'src/constants/database';
import { MongoId } from '../../common/types/mongo';

@Injectable()
export class UserService {
    constructor(@Inject(MODEL.USER) private readonly userModel: Model<IUserDocument>) {}

    async findAll(query: Partial<Paging>): Promise<IUserDocument[]> {
        const limit = Number(query.limit) || 10;
        const page = Number(query.page) || 1;
        const skip = limit * (page - 1);
        const conditions = {};
        return await this.userModel.find(conditions).skip(skip).limit(limit).exec();
    }

    async findById(id: MongoId): Promise<IUserDocument> {
        return await this.userModel.findById(id);
    }

    async findByEmail(email: string): Promise<IUserDocument> {
        return await this.userModel.findOne({ email });
    }

    async create(data: TCreateUser): Promise<IUserDocument> {
        return await this.userModel.create({
            ...data,
            password: await hash(data.password),
        });
    }

    async update(id: MongoId, data: TUpdateUser): Promise<IUserDocument> {
        return await this.userModel.findByIdAndUpdate(id, data, { new: true }).exec();
    }

    async changePassword(user: IUserDocument, password: string): Promise<IUserDocument> {
        return await this.userModel
            .findByIdAndUpdate(user.id, { password: await hash(password) }, { new: true })
            .exec();
    }

    async remove(id: MongoId): Promise<string> {
        await this.userModel.deleteOne({ _id: id }).exec();
        return 'User have been deleted !';
    }
}
