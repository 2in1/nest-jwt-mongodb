import { Document, Model } from 'mongoose';
import { CreateUserDto, UpdateUserDto } from './dto/user.dto';

export interface IUser {
    email: string;
    password: string;
    name: string;
    role: string;
    phone_number?: string;
    status?: string;
}

export interface IUserDocument extends IUser, Document {}

export type IUserModel = Model<IUserDocument>;

export type TCreateUser = CreateUserDto;

export type TUpdateUser = UpdateUserDto;
