import { Injectable, Inject } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { Model } from 'mongoose';
import { IUserDocument } from '../../app/user/user.interface';
import { MODEL } from 'src/constants/database';
import { RedisService } from 'src/core/redis/redis.service';
import { error401Response } from 'src/services/response.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        @Inject(MODEL.USER) private readonly userModel: Model<IUserDocument>,
        private readonly redis: RedisService,
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: process.env.JWT_SECRET,
        });
    }

    async validate(payload: any) {
        const token = await this.redis.get(payload.sub);
        if (!token) {
            return error401Response();
        }
        const user = await this.userModel.findById(payload.sub);
        return user;
    }
}
