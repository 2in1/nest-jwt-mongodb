import { JwtAuthGuard } from './../../core/guards/jwt.guard';
import { UserService } from './user.service';
import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { Paging } from 'src/common/types/paging';
import { CreateUserDto, UpdateUserDto, ChangePasswordDto } from './dto/user.dto';
import { MongoId } from '../../common/types/mongo';
import { IUserDocument } from './user.interface';
import { verify } from '../../services/crypto.service';
import { successResponse, error400Response, error404Response, IResponse } from 'src/services/response.service';
import { User } from 'src/core/decorators/user.decorator';

@UseGuards(JwtAuthGuard)
@Controller('user')
export class UserController {
    constructor(private readonly service: UserService) {}

    @Get()
    async index(@Query() query: Partial<Paging>): Promise<IResponse> {
        const response = await this.service.findAll(query);
        return successResponse(response);
    }

    @Get(':id')
    async find(@Param('id') id: MongoId): Promise<IResponse> {
        const user = await this.service.findById(id);
        if (!user) return error404Response('User not found !');
        return successResponse(user);
    }

    @Post()
    async create(@Body() data: CreateUserDto) {
        const user = await this.service.findByEmail(data.email);
        if (user) return error400Response('Email has exist !');

        const response = await this.service.create(data);
        return successResponse(response);
    }

    @Put('change-password')
    async changePassword(@User() user: IUserDocument, @Body() data: ChangePasswordDto) {
        const isMatch = await verify(data.password, user.password);
        if (!isMatch) return error400Response('Password incorrect !');

        const response = await this.service.changePassword(user, data.password);
        return successResponse(response);
    }

    @Put(':id')
    async update(@Param('id') id: MongoId, @Body() data: UpdateUserDto) {
        const user = await this.service.findById(id);
        if (!user) return error400Response('User not found !');

        const response = await this.service.update(id, data);
        return successResponse(response);
    }

    @Delete(':id')
    async remove(@Param('id') id: MongoId) {
        const data = await this.service.findById(id);
        if (!data) return error400Response('User not found !');
        const response = await this.service.remove(id);
        return successResponse(response);
    }
}
