import * as log4js from 'log4js';

const logLayout = {
    type: 'colored',
    pattern: '%d{yyyy-MM-dd hh:mm:ss.SSS} %p %z %c %m',
};

const logConfig = {
    console: {
        enable: true,
        level: 'debug',
    },
    defaultLevel: 'debug',
    file: {
        compress: false,
        app: 'logs/app.log',
        error: 'logs/error.log',
        access: 'logs/access.log',
        format: '.yyyy-MM-dd',
    },
    appenders: ['CONSOLE', 'FILE', 'ERROR_ONLY'],
};

log4js.configure({
    appenders: {
        FILE: {
            type: 'dateFile',
            filename: logConfig.file.app,
            pattern: logConfig.file.format,
            level: 'trace',
            layout: logLayout,
            compress: logConfig.file.compress,
            daysToKeep: 90,
        },
        CONSOLE: {
            type: 'stdout',
            layout: logLayout,
            level: 'trace',
        },
        FILE_ERROR: {
            type: 'dateFile',
            filename: logConfig.file.error,
            pattern: logConfig.file.format,
            level: 'trace',
            layout: logLayout,
            compress: logConfig.file.compress,
            daysToKeep: 90,
        },
        ERROR_ONLY: {
            type: 'logLevelFilter',
            appender: 'FILE_ERROR',
            level: 'error',
        },
    },
    categories: {
        default: {
            appenders: logConfig.appenders,
            level: logConfig.defaultLevel,
        },
    },
});
/**
 * @example
 * logger.trace("message");
 * logger.debug("message");
 * logger.info("message");
 * logger.warn("message");
 * logger.error("message");
 * logger.fatal("message");
 */

export const Logger = log4js.getLogger('Logger');
