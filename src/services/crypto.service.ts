import * as bcrypt from 'bcrypt';

export async function hash(text: string): Promise<string | null> {
    try {
        const salt = await bcrypt.genSalt();
        return await bcrypt.hash(text, salt);
    } catch (err) {
        console.log('bcrypt hash', err);
        return null;
    }
}

export async function verify(password: string, hashed: string): Promise<boolean> {
    try {
        return await bcrypt.compare(password, hashed);
    } catch (err) {
        console.log('bcrypt verify', err);
        return false;
    }
}
