import * as jwt from 'jsonwebtoken';

/**
 * Create temporary token for setting a password action.
 *
 * @param {object} payload
 * @param {string} secret - jwt's secret message
 * @param options
 * @returns {string} signed JWT
 */
export function makeJWT(payload: any, options?: jwt.SignOptions): string {
    const secret = process.env.JWT_SECRET;
    return jwt.sign(payload, secret, options);
}

/**
 * Decodes a token created by makeJWT.
 *
 * @param {string} token
 * @param {string} secret - jwt's secret message
 * @returns {object} payload in JWT. If verification fails, an error is thrown.
 */
export function verifyJWT(token: string, secret = process.env.JWT_SECRET): Promise<string | unknown> {
    return new Promise(async (resolve, reject) => {
        try {
            const decodeToken = await jwt.verify(token, secret);
            resolve(decodeToken);
        } catch (error) {
            reject(error);
        }
    });
}
