import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { DatabaseModule } from 'src/database/database.module';
import { UserProviders } from 'src/models/user.model';

@Module({
    imports: [DatabaseModule],
    controllers: [UserController],
    providers: [UserService, UserProviders],
    exports: [UserService],
})
export class UserModule {}
