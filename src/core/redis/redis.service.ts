import { Injectable, Inject, CACHE_MANAGER } from '@nestjs/common';
import { Cache } from 'cache-manager';

@Injectable()
export class RedisService {
    constructor(@Inject(CACHE_MANAGER) private readonly cache: Cache) {}

    async get(key): Promise<string> {
        return await this.cache.get(this.getName(key));
    }

    async set(key, value): Promise<void> {
        await this.cache.set(this.getName(key), value);
    }

    async del(key): Promise<number> {
        return await this.cache.del(this.getName(key));
    }

    async reset(): Promise<void> {
        await this.cache.reset();
    }

    getName(key: string): string {
        return `${process.env.APP_NAME}_${key}`;
    }
}
