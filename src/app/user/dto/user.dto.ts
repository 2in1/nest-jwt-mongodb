import { IsNotEmpty, IsString, IsOptional, IsEnum, IsEmail, MinLength, MaxLength } from 'class-validator';
import { UserRole, UserStatus } from 'src/constants/user';

export class CreateUserDto {
    @IsNotEmpty()
    @IsString()
    @IsEmail()
    readonly email: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(4)
    @MaxLength(20)
    readonly password: string;

    @IsString()
    readonly name: string;

    @IsEnum(UserRole)
    readonly role: string;

    @IsString()
    @IsOptional()
    readonly phone_number?: string;
}

export class UpdateUserDto {
    @IsString()
    @IsOptional()
    readonly name?: string;

    @IsString()
    @IsOptional()
    readonly phone_number?: string;

    @IsString()
    @IsOptional()
    @IsEnum(UserStatus)
    readonly status?: string;

    @IsString()
    @IsOptional()
    @IsEnum(UserRole)
    readonly role?: string;
}

export class ChangePasswordDto {
    @IsNotEmpty()
    @IsString()
    readonly password: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(4)
    @MaxLength(20)
    readonly newPassword: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(4)
    @MaxLength(20)
    readonly confirmPassword: string;
}
