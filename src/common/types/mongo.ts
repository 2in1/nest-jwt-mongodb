import { IsMongoId } from 'class-validator';

export class MongoId {
    @IsMongoId()
    readonly id: string;
}
