export enum UserStatus {
    ACTIVE = 'active',
    BLOCKED = 'blocked',
}

export enum UserRole {
    ADMIN = 'admin',
    USER = 'user',
}
