export type Paging = {
    limit: number;
    page: number;
};
