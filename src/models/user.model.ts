import * as mongoose from 'mongoose';
import { UserRole, UserStatus } from '../constants/user';
import { MODEL, DATABASE_CONNECTION } from '../constants/database';
import { IUserDocument } from '../app/user/user.interface';

export const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        index: true,
    },

    password: {
        type: String,
        required: true,
    },

    name: {
        type: String,
        required: true,
    },

    role: {
        type: String,
        required: true,
        enum: UserRole,
        default: UserRole.USER,
    },

    status: {
        type: String,
        required: true,
        enum: UserStatus,
        default: UserStatus.ACTIVE,
    },

    phone_number: String,
});

export const UserModel = mongoose.model<IUserDocument>('User', UserSchema);

export const UserProviders = {
    provide: MODEL.USER,
    useFactory: (connection: mongoose.Connection) => connection.model('User', UserSchema),
    inject: [DATABASE_CONNECTION],
};
