import * as helmet from 'helmet';
import * as redisStore from 'cache-manager-redis-store';
import { Module, CacheModule, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { CoreModule } from './core/core.module';
import { ConfigModule } from '@nestjs/config';
import { MorganModule } from 'nest-morgan';
import { UserModule } from './app/user/user.module';
import { AuthModule } from './app/auth/auth.module';
import { LoggerMiddleware } from './core/middleware/logger.middleware';

@Module({
    imports: [
        ConfigModule.forRoot(), // add environment
        MorganModule,
        CacheModule.register({
            store: redisStore,
            host: 'localhost',
            port: 6379,
        }),
        CoreModule,
        UserModule,
        AuthModule,
    ],
})
export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer.apply(helmet(), LoggerMiddleware).forRoutes('user');
    }
}
