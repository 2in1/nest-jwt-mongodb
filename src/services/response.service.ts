import { Response } from 'src/constants/error';
import { HttpException, HttpStatus } from '@nestjs/common';
import { Logger } from './logger.service';

const response = new Response();

export interface IResponse {
    statusCode: number;
    message: string;
    data?: any;
    error?: any;
}

/**
 * Makes an object to be returned as the JSON-encoded body of an http response for an error.
 *
 * @param {any} status
 * @param {string} message - user-friendly message describing the error
 * @param {object} error? - additional error information
 * @param {object} data? - additional error information
 * @returns {object}
 */

export function errorResponse(statusCode: number, message: string, error?: any): IResponse {
    Logger.error('ERROR Response |', message);
    throw new HttpException({ message, error }, statusCode);
}

export function successResponse(data): IResponse {
    return {
        statusCode: HttpStatus.OK,
        message: response.SUCCESSFUL,
        data,
    };
}

export function error500Response(message?: string, data?: any): IResponse {
    return errorResponse(HttpStatus.INTERNAL_SERVER_ERROR, message || response.INTERNAL_SERVER_ERROR, data);
}

export function error400Response(message?: string, data?: any): IResponse {
    return errorResponse(HttpStatus.BAD_REQUEST, message || response.BAD_REQUEST_ERROR, data);
}

export function error401Response(message?: string, data?: any): IResponse {
    return errorResponse(HttpStatus.UNAUTHORIZED, message || response.UNAUTHORIZED_ERROR, data);
}

export function error404Response(message?: string, data?: any): IResponse {
    return errorResponse(HttpStatus.NOT_FOUND, message || response.NOT_FOUND_ERROR, data);
}
